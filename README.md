# check_domain

nagios plugin for checking domain validity.

# install requirements

```
pip install -r requirements.txt
```

# Place in commands.cfg file:

```
define command{
        command_name    check_domain
        command_line    $USER1$/check_domain -H $HOSTADDRESS$
}
```

# Add to your service:

```
define service {
    use                    generic-service
    host_name              blahblah.com
    service_description    Check domain validity
    check_command          check_domain!-H blahblah.com
}
```
